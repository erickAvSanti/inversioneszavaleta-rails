class AddDefaultImageFieldOnProductImages < ActiveRecord::Migration[6.0]
  def change
    add_column :product_images, :is_default, :boolean, default: false, null: false
  end
end
