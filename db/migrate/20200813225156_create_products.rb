class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :prod_name, limit: 100, index: {name: :prod_name_idx, unique: false}, null: false
      t.text :prod_desc
      t.decimal :prod_price, precision: 7, scale: 2, default: 0, null: false
      t.decimal :prod_price_off, precision: 7, scale: 2, default: 0, null: false

      t.timestamps
    end
  end
end
