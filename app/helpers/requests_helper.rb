require 'net/http'
require 'uri'
require 'json'
module RequestsHelper
    def self.default_headers

		headers = {}
		headers['Content-Type'] = 'application/x-www-form-urlencoded'
		headers['Accept'] = 'application/json, text/plain, */*'
		headers['Accept-Encoding'] = 'gzip, deflate, br'
		headers['Accept-Language'] = 'es-ES,es;q=0.9,en;q=0.8'
		headers['Connection'] = 'keep-alive'
		headers['Sec-Fetch-Mode'] = 'cors'
		headers['Sec-Fetch-Site'] = 'same-origin'
		headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
		headers

	end

    def self.mix_headers(from, to)
        return {} if from.blank?
        from.each do |kk,vv|
            if vv.present?
                to[kk] = vv
            else
                to.delete kk
            end
        end
        to
    end
    
    def self.post_request(uri, custom_headers, data = nil)
		headers = mix_headers custom_headers, default_headers
		uri = URI(uri) if uri.kind_of? String

		puts "POST request = #{uri.to_s}"
		puts "Headers = #{headers.to_s}"
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = uri.scheme == 'https'
		res = {}
		http.start do |http|
			req = Net::HTTP::Post.new(uri)
			headers.each { |k,v| req[k] = v }
			if data.kind_of? String
				req.body = data
			else
				req.set_form_data(data) if data.present?
			end

			response = http.request req
			puts "response code = #{response.code}"
			res[ 'response' ] = response
			res[ 'code' ] = response.code
			res[ 'body' ] = response.body
            res['cookie'] = response['Set-Cookie']
			res[ 'Location' ] = response['Location'] if response.key? 'Location'
		  

        end
		res
    end
    
    def self.get_request(uri, custom_headers, data = nil)
		headers = mix_headers custom_headers, default_headers
		uri = URI(uri) if uri.kind_of? String

		puts "GET request = #{uri.to_s}"
		puts "Headers = #{headers.to_s}"
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = uri.scheme == 'https'
		res = {}
		http.start do |http|
			req = Net::HTTP::Get.new(uri)
			headers.each { |k,v| req[k] = v }
			response = http.request req
			puts "response code = #{response.code}"
			res[ 'response' ] = response
			res[ 'code' ] = response.code
			res[ 'body' ] = response.body
			res[ 'Location' ] = response['Location'] if response.key? 'Location'
		end
		res
	end

end
