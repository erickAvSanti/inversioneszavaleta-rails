Rails.application.routes.draw do
	scope '/api-adm' do 
		resources :products
		resources :categories 
		put '/products/set-categories/:id', to: 'products#set_categories'
		put '/products/set-images/:id', to: 'products#set_images'
		put '/products/set-default-image/:id/:image_id', to: 'products#set_default_image'
		delete '/products/remove-image/:id/:image_id', to: 'products#remove_image'
		get '/products/images/:id', to: 'products#get_images'

		scope '/product-tags' do

			get '/:product_id', to: 'product_tags#get_tags'
			post '/', to: 'product_tags#create'
			put '/:id', to: 'product_tags#update'
			delete '/:id', to: 'product_tags#destroy'
		end
		scope '/users' do 
			get '', to: 'users#index'
			get '/:id', to: 'users#show'
			post '/', to: 'users#create'
			put '/:id', to: 'users#update'
			delete '/:id', to: 'users#destroy'
		end
		scope '/web-properties' do 
			get '', to: 'web_properties#index'
			get '/:id', to: 'web_properties#show'
			post '/', to: 'web_properties#create'
			put '/:id', to: 'web_properties#update'
			delete '/:id', to: 'web_properties#destroy'
		end

		post '/login', to: 'authorization#login'
		get '/auto_login', to: 'authorization#auto_login'
	end
	
	# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
	get '/', to: 'website#index'

	get '/categories/:category_slug', to: 'website#parse_category_slug'
	get '/product/:product_slug', to: 'website#parse_product_slug'

	get '/terminos-y-condiciones', to: 'website#terms_and_conditions'
	get '/politicas-de-privacidad', to: 'website#privacy_policy'
	get '/contactanos', to: 'website#contact_us'
	get '/carrito', to: 'website#cart'
	get '/busqueda', to: 'website#search'
	post '/guest/orders', to: 'website#request_order'

end
