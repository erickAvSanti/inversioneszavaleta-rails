class ProductTag < ApplicationRecord
    belongs_to :product, foreign_key: :product_id
    validates :tag_key, presence: {message: 'Indique el título de la etiqueta'}
    validates :tag_value, presence: {message: 'Indique la descripción de la etiqueta'}
    validates_length_of :tag_value, minimum: 1, maximum: 200, allow_blank: false, too_long: 'Máximo 200 caracteres de la descripción de la etiqueta', too_short: 'Mínimo un caracter de la descripción de la etiqueta'

    public
    def self.getAll(params)
        if params[:product_id].present?
            product = Product.find_by_id params[:product_id]
            product.product_tags
        else
            []
        end
    end

end
