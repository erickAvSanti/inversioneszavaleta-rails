module ApplicationHelper
    def self.storage_path(dir = nil)
        dir = "storage#{ dir.present? ? "/#{dir}" : '' }"
        Rails.root.join(dir)
    end
    def self.make_storage_path(dir, mode = 0777)
        dir = self.storage_path(dir)
        FileUtils.mkdir_p(dir, mode: mode) if !dir.directory?
        dir if dir.directory?
    end

    def self.datetime_utc_hyphen
        DateTime.now.utc.strftime '%d_%m_%Y__%H_%M_%S'
    end

    def self.datetime_utc_human_r
        DateTime.now.utc.strftime '%d/%m/%Y %H:%M:%S'
    end

end
