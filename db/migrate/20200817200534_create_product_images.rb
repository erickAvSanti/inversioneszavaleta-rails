class CreateProductImages < ActiveRecord::Migration[6.0]
  def change
    create_table :product_images do |t|
      t.bigint :product_id, unsigned: true, null: false
      t.string :file_name, limit: 120, null: false
      t.string :file_hash, limit: 100, null: false, index: {name: :file_hash_idx, unique: true}
      t.timestamps
    end
  end
end
