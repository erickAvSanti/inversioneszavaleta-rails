class UsersController < ApiController
    before_action :set_user, only: [
      :show, 
      :update, 
      :destroy
    ]
  
    def index
        @records = User.getAll(params)
        render json: @records
    end

    def show
        render json: @user
    end

    # REGISTER
    def create
        @user = User.create(user_params)
        if @user.valid?
            head :ok
            #encoded = encode_token({user_id: @user.id})
            #render json: {user: @user.without_digest, token: encoded[:token], expires_in: encoded[:expires_in], expires_at: encoded[:expires_at]}
        else
            render json: {error: "Invalid username or password"}, status: :bad_request
        end
    end
    def update
        if @user.update!(user_params)
            @user.update!({password: params[:password]}) if params[:password].present?            
        end
        head :ok
    rescue
        render json: @user.errors, status: :unprocessable_entity
    end
  
    private
    def set_user
      @user = User.find_by_id(params[:id])
      if @user.blank? 
        head 404
      end
    end
  
    def user_params
        permit_arr = [:full_name,:username]
        permit_arr << :password if params[:password].present?
        params.require(:user).permit(permit_arr)
    end
end
