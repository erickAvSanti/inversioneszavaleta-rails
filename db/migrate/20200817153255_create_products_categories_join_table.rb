class CreateProductsCategoriesJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_table :products_categories, id: false do |t|
      t.bigint :product_id, unsigned: true
      t.bigint :category_id, unsigned: true
    end
 
    #add_index :products_categories, :product_id
    #add_index :products_categories, :category_id

    add_index :products_categories, [:product_id, :category_id], unique: true
  end
end
