class Category < ApplicationRecord
    has_many :categories, foreign_key: :cat_id
    belongs_to :category, foreign_key: :cat_id, optional: true

    has_and_belongs_to_many :products, join_table: :products_categories

    before_destroy :can_destroy

    public 
    def self.all_with_nested
        arr = []
        records = Category.all
        records.each do |record|
            next if record.cat_id.present?
            obj = Category.category_to_map(record,2)
            arr << obj
            end
        arr
    end

    def products_with_images
        Product::with_images(products)
    end

    def website_url
        "/categories/#{id}--#{cat_name.parameterize}"
    end

    def get_ancestors(reverse = false)
        ancestors = [self]
        ancestors << category if category.present?
        ancestors << category.category if category.present? and category.category.present?
        reverse ? ancestors.reverse : ancestors
    end

    private 
    def can_destroy 
        if categories.any?
            throw 'cant destroy category'
        end
    end

    def self.category_to_map(category, level = 3)
        obj = {
            id: category.id,
            cat_name: category.cat_name.split.map(&:capitalize).join(' '),
            cat_id: category.cat_id,
            website_url: category.website_url,
        }
        obj[:children] = []
        category.categories.each do |subcat|
            obj[:children] << Category.category_to_map(subcat, level - 1)
        end if level > 0 and category.categories.present?
        obj
    end
end
