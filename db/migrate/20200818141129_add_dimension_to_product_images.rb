class AddDimensionToProductImages < ActiveRecord::Migration[6.0]
  def change
    add_column :product_images, :img_dim_width, :integer, unsigned: true, null: false, default: 0
    add_column :product_images, :img_dim_height, :integer, unsigned: true, null: false, default: 0
  end
end
