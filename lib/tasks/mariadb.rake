=begin
export DB_HOST=hostname_or_ip
export DB_PORT=3306
export DB_DATABASE=my_database
export DB_USERNAME=username
export DB_PASSWORD=password
=end
namespace :mariadb do
    task backupdb: [:environment] do
        directory = ApplicationHelper.make_storage_path "mariadb/rake_backups"
        file_path = "#{directory}/mariadb__#{ApplicationHelper.datetime_utc_hyphen}.sql"
        command = "mysqldump -u #{ENV.fetch('DB_USERNAME')} #{ ENV.fetch('DB_PASSWORD').present? ? "-p #{ENV.fetch('DB_PASSWORD')}" : '' } #{ ENV.fetch("DB_HOST").present? ? "-h #{ENV.fetch("DB_HOST")}" : '' } #{ ENV.fetch('DB_DATABASE') } --skip-tz-utc > #{file_path}"
        Rails.logger.info command
        command = `#{command}`
    end
    task :createdb, [:name] => [:environment] do |task, args|
        charset = 'utf8mb4'
        collation = 'utf8mb4_unicode_ci'
        query = "CREATE DATABASE IF NOT EXISTS #{args.name} CHARACTER SET #{charset} COLLATE #{collation};"
        command = "mysql -u #{ENV.fetch('DB_USERNAME')} #{ ENV.fetch('DB_PASSWORD').present? ? "-p #{ENV.fetch('DB_PASSWORD')}" : '' } #{ ENV.fetch("DB_HOST").present? ? "-h #{ENV.fetch("DB_HOST")}" : '' } -e '#{query}'"
        Rails.logger.info command
        command = `#{command}`
    end
end
