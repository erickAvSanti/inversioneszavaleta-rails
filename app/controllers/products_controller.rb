class ProductsController < ApiController
  before_action :set_product, only: [
    :show, 
    :update, 
    :set_categories,
    :remove_image, 
    :get_images, 
    :set_images,
    :set_default_image,
    :destroy
  ]

  # GET /products
  # GET /products.json
  def index
    @products = Product.getAll(params)
    render json: @products
  end

  # GET /products/1
  # GET /products/1.json
  def show
    obj = @product.attributes
    #obj['categories'] = @product.categories
    obj['categories_ids'] = @product.categories.pluck(:id)
    render json: obj
  end

  # GET /products/new
  #def new
    #@product = Product.new
  #end

  # GET /products/1/edit
  #def edit
  #end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    if @product.save
      @product.set_folder
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    if @product.update(product_params)
      @product.set_folder
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end

=begin

    if @product.set_props(product_params, params)
      record = @product.attributes
      record[:categories] = @product.categories.pluck(:id,:cat_name).map { |id, name| { id: id, cat_name: name } }
      render json: record
    else
      render json: @product.errors, status: :unprocessable_entity
    end
=end
  end

  def set_categories
    if @product.set_categories_ids(params)
      render json: nil, status: :ok
    else
      render json: nil, status: :unprocessable_entity
    end
  end

  def set_images
    if @product.set_images(params)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def set_default_image
    if @product.set_default_image(params[:image_id])
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def remove_image
    if @product.remove_image(params)
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def get_images
    render json: @product.my_images
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    begin
      @product.destroy_dependencies if params[:force_delete].present?
      @product.destroy
      head :ok
    rescue => exception
      render json: {message: exception.message}, status: :bad_request
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find_by_id(params[:id])
      if @product.blank? 
        head 404
      end
    end

    # Only allow a list of trusted parameters through.
    def product_params
      permit_arr = [:prod_name]
      permit_arr << :prod_desc if params[:prod_desc].present?
      permit_arr << :prod_price if params[:prod_price].present?
      permit_arr << :prod_price_off if params[:prod_price_off].present?
      permit_arr << :show_prod_price_off if !params[:show_prod_price_off].nil?
      params.require(:product).permit(permit_arr)
    end
end
