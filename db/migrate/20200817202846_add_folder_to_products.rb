class AddFolderToProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :folder, :string, limit: 100, index: {name: :folder_idx, unique: true}
  end
end
