class WebPropertiesController < ApiController
	before_action :set_web_property, only: [
		:update, 
		:destroy
	]
	def index
		render json: WebProperty.getAll(params)
	end
	
	def create
		@web_property = WebProperty.new(web_property_params)
		if @web_property.save
			head :created
		else
			render json: @web_property.errors, status: :unprocessable_entity
		end
	end
	
	def update
		if @web_property.update(web_property_params)
			head :ok
		else
			render json: @web_property.errors, status: :unprocessable_entity
		end
	end
	
	def destroy
		@web_property.destroy
		head :ok
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_web_property
			@web_property = WebProperty.find_by_id(params[:id])

			if @web_property.blank? 
				head :not_found
			end
		end

		# Only allow a list of trusted parameters through.
		def web_property_params
			permit_arr = [:prop_key, :prop_desc, :prop_value]
			params.require(:web_property).permit(permit_arr)
		end
	
end
