class GuestOrdersMailer < ApplicationMailer
    def request_order
        delivery_options = {
            user_name: ENV.fetch('SMTP_USER'),
            password: ENV.fetch('SMTP_PASSWORD'),
            address: ENV.fetch('SMTP_HOST'),
        }
        @form = params[:form]
        @products = params[:products]
        mail(to: 'ecavaloss@gmail.com', subject: 'rails test',delivery_method_options: delivery_options)
    end
end
