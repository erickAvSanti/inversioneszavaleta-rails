const { default: Swal } = require("sweetalert2")

$(document).on('turbolinks:load',function(){
    setTimeout(()=>{
        $(".owl-carousel").owlCarousel({
            center: true,
            loop: true,
            items: 2,
            autoWidth: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:true
                }
            }
        })
    },400)

    const modal_shopping_cart = jQuery('#modal-shopping-cart')

    jQuery('.icofont-search').click(function(evt){
        jQuery('#modal-search').modal('show')
    })

    jQuery('.btn-cart').click(function(evt){
        const obj = jQuery(this)
        let product = {}
        try {
            product = jQuery.parseJSON(obj.attr('product'))
        } catch (e) {}

        let product_found = null
        const products_arr = get_shopping_cart_products()
        if(
            product.id && 
            products_arr && 
            Array.isArray(products_arr)
        ){
            for(const prod of products_arr){
                if(prod.id == product.id){
                    product_found = prod
                    break
                }
            }
        }
        if(!product_found){
            product.buy_quantity = 1
            product.first_image_url = obj.attr('product-first-image-url')
            products_arr.push(product)
        }else{
            product_found.prod_price = product.prod_price
            product_found.prod_name = product.prod_name
        }
        put_shopping_cart_products(products_arr)
        show_modal_shopping_cart_modal(modal_shopping_cart,products_arr,product_found ? product_found : product)
    })
    
    modal_shopping_cart.on('shown.bs.modal', function () {
            
    })

    jQuery('.cart-list .btn-remove').click(function(evt){
        const obj = jQuery(this)
        const product_id = obj.attr('item-id')
        const product_name = obj.attr('product-name')
        if(product_id && product_name){
            Swal.fire({
                title: `Retirar producto '${product_name}' del carrito?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: 'rgb(128 128 128)',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.value) {
                    jQuery(`.cart-row-item[item-id=${product_id}]`).detach()
                    remove_product_from_cart(product_id)
                    process_shopping_cart_items()
                }
            })
        }
    })
    process_shopping_cart_items()
    quantityActionEvents()
    processSubmitShoppingCart()
    jQuery('.btn-go-to-cart').click(function(evt){
        if(window.location.assign){
            window.location.assign("/carrito")
        }else{
            window.location.href = "/carrito"
        }
    })
    menu_responsive()
    product_images_preview()
})
function quantityActionEvents(){
    jQuery('.cart-list .add-quantity').click(function(evt){
        const obj = jQuery(this)
        const product_id = obj.attr('item-id')
        if(product_id){
            const current_quantity_element = jQuery(`.cart-list .current-quantity[item-id=${product_id}]`)
            if(current_quantity_element){
                let quantity = getIntVal(current_quantity_element.parent().parent().attr('quantity')) || 1
                quantity += 1
                current_quantity_element.parent().parent().attr('quantity',quantity)
                jQuery(`.current-quantity[item-id=${product_id}]`).html(quantity)
                jQuery(`.cart-row-item[item-id=${product_id}]`).attr('prod-quantity',quantity)
                process_shopping_cart_items()
            }
        }
    })
    jQuery('.cart-list .dec-quantity').click(function(evt){
        const obj = jQuery(this)
        const product_id = obj.attr('item-id')
        if(product_id){
            const current_quantity_element = jQuery(`.cart-list .current-quantity[item-id=${product_id}]`)
            if(current_quantity_element){
                let quantity = getIntVal(current_quantity_element.parent().parent().attr('quantity')) || 1
                if(quantity>1){
                    quantity -= 1
                    current_quantity_element.parent().parent().attr('quantity',quantity)
                    jQuery(`.current-quantity[item-id=${product_id}]`).html(quantity)
                    jQuery(`.cart-row-item[item-id=${product_id}]`).attr('prod-quantity',quantity)
                    process_shopping_cart_items()
                }
            }
        }
    })
}
function process_shopping_cart_items(){
    const total = calc_total_shopping_cart_items()
    jQuery('#shopping-cart-total').html(`Total: ${total.toFixed(2)}`)
}

function calc_total_shopping_cart_items(){
    let sum = 0
    jQuery('.cart-row-item').each((idx,it)=>{
        const obj = jQuery(it)
        const product_id = obj.attr('item-id')
        const price = getFloatVal(obj.attr('prod-price'))
        const quantity = getFloatVal(obj.attr('prod-quantity'))
        if(price && quantity){
            jQuery(`.sub-total-item[item-id=${product_id}]`).html( (price*quantity).toFixed(2) )
            sum += price * quantity
        }
    })
    return sum
}
function getFloatVal(str){
    try{
        return parseFloat(str)
    }catch(e){
        return null
    }
}
function getIntVal(str){
    try{
        return parseInt(str)
    }catch(e){
        return null
    }
}

function remove_product_from_cart(product_id){
    products_arr = get_shopping_cart_products()
    products_arr = products_arr.filter( product_arr => product_arr['id'] != product_id)
    if(products_arr.length == 0){
        window.location.reload()
    }
    put_shopping_cart_products(products_arr)
}

function get_shopping_cart_products(){
    const products_json = getCookie('products')
    let products_arr = []
    try{
        products_arr = jQuery.parseJSON(products_json)
    }catch(e){}
    return products_arr
}
function put_shopping_cart_products(products_arr){
    setCookie('products',JSON.stringify(products_arr))
}

function show_modal_shopping_cart_modal(modal_shopping_cart,products_arr,product){
    modal_shopping_cart.find('.modal-body').html('')
    modal_shopping_cart.modal('show')
    let str = `
        <div class="alert alert-warning" role="alert">
        Indique la cantidad deseada. Nuestra área de ventas le confirmará si contamos lo solicitado al recepcionar su pedido.
        </div>
        <table class="modal-shopping-table">
            <tr>
                <td class="modal-img-container">
                    <img src="${product.first_image_url}" />
                </td>
                <td>
                    <div>${product.prod_name}</div>
                </td>
                <td>
                    <div>${product.prod_price}</div>
                </td>
                <td>
                    <div>
                        <label>Cantidad:</label><br />
                        <input type="number" value="${product.buy_quantity}" min="1" max="1000" style="width:60px"/>
                    </div>
                </td>
            </tr>
        </table>
    `
    modal_shopping_cart.find('.modal-body').html(str)
    setModalShoppingCartEvents(products_arr,product)
}
function setModalShoppingCartEvents(products_arr,product){
    jQuery('.modal-shopping-table input').blur(function(evt){
        const obj = jQuery(this)
        product.buy_quantity = obj.val()
        setCookie('products',JSON.stringify(products_arr))
    })
}

function setCookie(cname, cvalue, exdays = 7) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
function processSubmitShoppingCart(){
    
}
function menu_responsive(){
    jQuery('#btn-menu').click(function(evt){
        const obj = jQuery(this)
        jQuery('#menu-container').toggleClass('open')
    })
    jQuery('#menu-container button.close').click(function(evt){
        jQuery('#menu-container').toggleClass('open')
    })
}

function product_images_preview(){
    jQuery(".products-list-preview .img-preview").click(function(evt){
        const obj = jQuery(this)
        const img_url = jQuery(obj).attr('img-url')
        jQuery('#modal-img-preview img').attr('src',img_url)
        jQuery('#modal-img-preview').modal('show')
    })
}