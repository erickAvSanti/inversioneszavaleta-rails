json.extract! product, :id, :prod_name, :prod_desc, :prod_price, :prod_price_off, :created_at, :updated_at
json.url product_url(product, format: :json)
