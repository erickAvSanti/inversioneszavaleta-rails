class WebProperty < ApplicationRecord
    validates :prop_key, presence: {message: 'Indique el identificador de la propiedad web'}
    validates_length_of :prop_key, minimum: 1, maximum: 50, allow_blank: false, too_long: 'Máximo 40 caracteres del identificador de la propiedad web', too_short: 'Mínimo un caracter del identificador de la propiedad web'
    
    validates :prop_value, presence: {message: 'Indique el valor de la propiedad web'}
    validates_length_of :prop_value, minimum: 1, maximum: 400, allow_blank: false, too_long: 'Máximo 400 caracteres del valor de la propiedad web', too_short: 'Mínimo un caracter del valor de la propiedad web'
    
    validates :prop_desc, length: { maximum: 400, message: "Máximo 400 caracteres de la descripción de la propiedad" }, allow_blank: true 
    public
    def self.getAll(params)
        WebProperty.all
    end

    def self.toKeysMap()
        records = {}
        WebProperty.all.each { |record| records[record.prop_key] = record.attributes }
        records
    end

    def self.get(key, only_value = true)
        record = WebProperty.where(prop_key: key).take
        return record.prop_value if record and only_value
        record
    end

end
