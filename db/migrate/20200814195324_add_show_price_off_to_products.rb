class AddShowPriceOffToProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :show_prod_price_off, :boolean, default: false
  end
end
