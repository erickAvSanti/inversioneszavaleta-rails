class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :cat_name, limit: 70, index: {name: 'cat_name_unique', unique: true}, null: false
      t.bigint :cat_id, unsigned: true, null: true, default: nil
      t.timestamps
    end
  end
end
