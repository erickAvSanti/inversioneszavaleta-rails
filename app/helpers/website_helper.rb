require 'net/http'
require 'uri'
require 'json'

module WebsiteHelper
    def self.process_cart(cookies)
        products_arr = JSON.parse(cookies[:products]) rescue []
        products = []
        products_json_map = {}
        products_arr.each do |product_json|
            product = Product::find_by_id product_json['id']
            if product.present?
                products_json_map[product.id] = product_json
                products << product 
            end
        end if products_arr.present?
        products = Product::with_images(products)
        products.each do |product|
            product['buy_quantity'] = products_json_map[product['id']]['buy_quantity'] if products_json_map[product['id']]['buy_quantity'].present?
        end if products.present?
        products
    end

    def self.post_request(uri, headers, data = nil)
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = uri.scheme == 'https'
		response = nil
		http.start do |http|
			req = Net::HTTP::Post.new(uri)
			headers.each { |k,v| req[k] = v }
			if data.kind_of? String
				req.body = data
			else
				req.set_form_data(data) if data.present?
			end

			begin
				response = http.request req
			rescue => error
				Rails.logger.info error.message
			end
		end
		response
    end
    
    def self.process_request_order(params)
        items = params[:items]
        products = []
        items.each do |it|
            next if it["qt"].blank? or it["id"].blank?
            product = Product.find_by_id(it["id"])
            products << product.attributes.merge({"quantity" => it["qt"]}) if product.present?
        end
        return false if products.blank?
        form_params = params.slice(:fullname, :phone, :email, :address)
        
        GuestOrdersMailer.with(form: form_params.permit(:fullname, :phone, :email, :address),products: products).request_order.deliver_later
        return true
    end

end
