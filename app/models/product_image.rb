class ProductImage < ApplicationRecord
    belongs_to :product, foreign_key: :product_id
    after_destroy :remove_image_file

    public 
    def file_exist?
        if product.present? and product.folder.present?
            file_path = Rails.root.join('public',"#{Product::UPLODAS_DIR}/#{product.folder}/#{file_hash}")
            return file_path.exist?
        end
        false
    end

    def dimension_defined?
        img_dim_width > 0 and img_dim_height > 0
    end

    def relative_url
        "#{Product::UPLODAS_DIR}/#{product.folder}/#{file_hash}"
    end

    private
    def remove_image_file
        if product.present? and product.folder.present?
            file_path = Rails.root.join('public',"#{Product::UPLODAS_DIR}/#{product.folder}/#{file_hash}")
            puts "Eliminando imagen = #{file_path}"
            file_path.delete
        end
    end
end
