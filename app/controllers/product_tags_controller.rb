class ProductTagsController < ApiController
	before_action :set_product_tag, only: [
		:update, 
		:destroy
	]
	def get_tags
		@product_tags = ProductTag.getAll(params)
		render json: @product_tags
	end
	
	def create
		@product_tag = ProductTag.new(product_tag_params)
		if @product_tag.save
			head :created
		else
			render json: @product_tag.errors, status: :unprocessable_entity
		end
	end
	
	def update
		if @product_tag.update(product_tag_params)
			head :ok
		else
			render json: @product_tag.errors, status: :unprocessable_entity
		end
	end
	
	def destroy
		@product_tag.destroy
		head :ok
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_product_tag
			@product_tag = ProductTag.find_by_id(params[:id])

			if @product_tag.blank? 
				head :not_found
			else
				if params[:product_id].present?
					if params[:product_id].to_i != @product_tag.product_id 
						head :bad_request
					end
				end
			end
		end

		# Only allow a list of trusted parameters through.
		def product_tag_params
			permit_arr = [:tag_key, :tag_value, :product_id]
			params.require(:product_tag).permit(permit_arr)
		end
	
end
