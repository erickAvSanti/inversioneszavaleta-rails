# Preview all emails at http://localhost:3000/rails/mailers/guest_orders_mailer
class GuestOrdersMailerPreview < ActionMailer::Preview
    def request_order
        form = {fullname: 'Erick', phone: '123456789', email: 'ecavaloss@gmail.com'}
        products = []
        GuestOrdersMailer.with(form: form, products: products).request_order
    end
end
