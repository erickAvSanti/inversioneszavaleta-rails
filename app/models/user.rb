class User < ApplicationRecord

    UPLODAS_DIR = 'uploads/users'
    PAGINATION_RECORDS_X_PAGE = 20

    has_secure_password
    validates :username, uniqueness: {message: 'Ya existe el usuario', case_sensitive: true} 

    public 

    def without_digest
        attr = attributes
        attr.delete 'password_digest'
        attr
    end
    

    def self.getAll(params)
        records_x_page = PAGINATION_RECORDS_X_PAGE
        current_page = 1
        records_x_page = params[:records_x_page].to_i if params[:records_x_page].present?
        records_x_page = PAGINATION_RECORDS_X_PAGE if records_x_page > 200

        current_page = params[:current_page].to_i if params[:current_page].present?

        records = User
        #records = records.where()....
        if params[:search].present?
            records = records.where("full_name LIKE '%#{params[:search]}%'")
        end
        total_records = records.count

        total_pages = ( total_records.to_f / records_x_page.to_f ).floor + ( total_records % records_x_page > 0 ? 1 : 0 )
        current_page = current_page > total_pages ? total_pages : (current_page > 0 ? current_page : 1)
        offset = (current_page - 1) * records_x_page
        offset = 0 if offset<0
        records = records.limit(records_x_page).offset(offset)
        
        arr = []
        records.each do |record|
            obj = record.attributes
            arr << obj
        end

        records = arr

        return {
            total_records: total_records,
            total_pages: total_pages,
            current_page: current_page,
            records_x_page: records_x_page,
            records: records
        }

    end
end
