require 'json'
namespace :login do
    task :test_default => [:environment] do 
        url = 'http://localhost:3000/login'
        headers = {'Content-Type' => 'application/json', 'Accept' => 'application/json'}
        data = {username: Env.fetch('USER_TEST_USERNAME'), password: Env.fetch('USER_TEST_PASSWORD')}
        request = RequestsHelper.post_request url, headers, data
        
        if request['code'].to_i == 200
            puts JSON.pretty_generate JSON.parse(request['body'])
        else
            puts JSON.pretty_generate request
        end
    end
end
