require 'net/http'
require 'uri'
require 'json'

class WebsiteController < ApplicationController
    before_action :get_common_values
    def index
        @products = Product::with_images(Product.all)
    end

    def parse_category_slug
        @category = Category.find_by_id params[:category_slug].to_i
        if @category.blank?
            redirect_to('/') 
        else
            @category_ancestors = @category.get_ancestors
            @products = @category.products_with_images
            #@products = @category.products
            render template: "website/categories"
        end
    end

    def parse_product_slug
        @product = Product.find_by_id params[:product_slug].to_i
        if @product.blank?
            redirect_to('/') 
        else
            render template: "website/product"
        end
    end

    def terms_and_conditions
        render template: "website/terms_and_conditions"
    end

    def privacy_policy
        render template: "website/privacy_policy"
    end

    def contact_us
        render template: "website/contact_us"
    end

    def cart
        @products = WebsiteHelper.process_cart(cookies)
        render template: "website/cart"
    end

    def search
        @products = Product::where("prod_name LIKE '%#{params[:query]}%' OR prod_desc LIKE '%#{params[:query]}%'")
        @products = Product::with_images(@products)
    end

    def request_order 
        puts params.to_s
        uri = URI('https://www.google.com/recaptcha/api/siteverify')
        res = WebsiteHelper.post_request(uri,{'Accept': 'application/json'},{secret: ENV.fetch('RECAPTCHA_SECRET'),response: params['g-recaptcha-response'],remoteip: request.remote_ip})
        Rails.logger.info "code: #{res.code}"
        Rails.logger.info "body: #{res.body}"

        if res.code.to_i == 200
            json = JSON.parse(res.body)
            if json['success'] == true
                Rails.logger.info "pass"
                WebsiteHelper.process_request_order(params)
                head :ok
            else
                render json: {msg: 'captcha error'}, status: :unprocessable_entity
            end
        else
            render json: {msg: 'captcha error'}, status: :bad_request
        end

    end

    private 
        def get_common_values
            @menu_category = Category.all_with_nested
        end
end
