class CreateWebProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :web_properties do |t|
      t.string :prop_key, limit: 50, null: false, index: {unique: true, name: :unique_prop_key}
      t.text :prop_desc
      t.text :prop_value
      t.timestamps
    end
  end
end
