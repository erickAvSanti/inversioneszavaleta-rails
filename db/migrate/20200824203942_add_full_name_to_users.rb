class AddFullNameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :full_name, :string, limit: 100, null: true, default: ''
  end
end
