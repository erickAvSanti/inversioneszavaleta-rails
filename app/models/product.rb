class Product < ApplicationRecord

    UPLODAS_DIR = 'uploads/products'
    PAGINATION_RECORDS_X_PAGE = 20

    has_and_belongs_to_many :categories, join_table: :products_categories
    has_many :product_images, foreign_key: :product_id
    has_many :product_tags, foreign_key: :product_id

    validates :prod_name, presence: { message: 'Indique el nombre del producto' }
    validates_length_of :prod_desc, minimum: 0, maximum: 400, allow_blank: true

    before_destroy :can_destroy

    private 
    def can_destroy
        raise "Este producto tiene etiquetas asociadas" if product_tags.any?
        raise "Este producto tiene imágenes asociadas" if product_images.any?
    end

    public 

    def set_default_image(product_image_id)
        product_image_id = product_image_id.to_i
        product_image = product_images.find(product_image_id)
        if product_image.present?
            ProductImage.transaction do
                product_images.each do |prod_img| 
                    prod_img.update!(is_default: prod_img.id == product_image_id)
                end
            end
            true
        else
            false
        end
    rescue => e
        Rails.logger.info e.message 
        false
    end

    def get_default_image
        product_images.where(is_default: true).take
    end

    def destroy_dependencies
        categories.clear
        product_tags.destroy product_tags
        product_images.destroy product_images
    end

    def self.with_images(products)
        arr = []
        products.each do |product|
            obj = product.attributes
            obj['record'] = product
            obj['images'] = product.my_images
            arr << obj
        end if products.any?
        arr
    end

    def website_url
        "product/#{id}--#{prod_name.parameterize}"
    end

    def first_image_url(request_base_url)
        return "#{request_base_url}/#{get_default_image.relative_url}" if get_default_image.present?
        return "#{request_base_url}/#{my_images[0][:relative_url]}" if my_images[0].present?
        return "#{request_base_url}/default.jpg"
    end

    def self.getAll(params)
        records_x_page = PAGINATION_RECORDS_X_PAGE
        current_page = 1
        records_x_page = params[:records_x_page].to_i if params[:records_x_page].present?
        records_x_page = PAGINATION_RECORDS_X_PAGE if records_x_page > 200

        current_page = params[:current_page].to_i if params[:current_page].present?

        records = Product
        #records = records.where()....
        if params[:search].present?
            records = records.where("prod_name LIKE '%#{params[:search]}%'")
        end
        total_records = records.count

        total_pages = ( total_records.to_f / records_x_page.to_f ).floor + ( total_records % records_x_page > 0 ? 1 : 0 )
        current_page = current_page > total_pages ? total_pages : (current_page > 0 ? current_page : 1)
        offset = (current_page - 1) * records_x_page
        offset = 0 if offset<0
        records = records.limit(records_x_page).offset(offset)
        
        arr = []
        records.each do |record|
            obj = record.attributes
=begin
            images = record.product_images
            arr_imgs = []
            images.each do |img|
                arr_imgs << img.attributes.merge({relative_url: img.relative_url})
            end
            obj['images'] = arr_imgs
=end
            obj['images'] = record.my_images
            arr << obj
        end

        records = arr

        return {
            total_records: total_records,
            total_pages: total_pages,
            current_page: current_page,
            records_x_page: records_x_page,
            records: records
        }

    end

    def set_categories_ids(params)
        Product.transaction do
            categories.clear
            params[:categories].each do |category_id|
                category = Category.find_by_id category_id
                categories << category if category.present?
            end if params[:categories].kind_of? Array
        end
        true
    rescue => e
        Rails.logger.info e.message
        false
    end

    def set_images(params)
        directory = create_folder_on_disk
        Product.transaction do
            params.each do |kparam,vparam|
                if vparam.kind_of? ActionDispatch::Http::UploadedFile
                    if vparam.content_type == 'image/jpeg' or vparam.content_type == 'image/jpg' or vparam.content_type == 'image/png'
                        new_file_name = "#{random_string}#{File.extname(vparam.original_filename)}"
                        new_file_path = "#{directory}/#{new_file_name}"
                        File.open(new_file_path, 'wb'){ |f| f.write( vparam.tempfile.read ) } if directory.directory?
                        if File.file? new_file_path
                            image_size = ImageSize.path(new_file_path)
                            img = ProductImage.new({
                                file_hash: new_file_name, 
                                file_name: vparam.original_filename, 
                                product_id: id,
                                img_dim_width: image_size.w,
                                img_dim_height: image_size.h,
                            })
                            img.save!
                        end
                    end
                end
            end
        rescue => e
            Rails.logger.info e.message
            return false
        end
        true
    end

    def set_folder
        if folder.blank? and persisted?
            update({folder: random_string(40)})
        end
    end

    def remove_image params 
        img = ProductImage.find_by_id(params[:image_id])
        return false if img.blank? or img.product_id != id
        img.destroy
        img.destroyed?
        #TODO
    end

    def my_images
        arr = []
        product_images.each do |product_image|
            obj = product_image.attributes.merge({relative_url: product_image.relative_url})
            arr << obj if product_image.file_exist? and product_image.dimension_defined?
        end if folder.present?
        arr
    end

    def images_files_not_deleted
        directory = Rails.root.join('public',"#{UPLODAS_DIR}/#{folder}")
        arr = []
        Dir.entries(directory).each do |file_hash|
            next if file_hash.to_s == '.' or file_hash.to_s == '..'
            prod_img = product_images.where(file_hash: file_hash).take
            arr << "#{directory}/#{file_hash}" if prod_img.blank?
        end
        arr
    end

    def remove_images_files_not_deleted
        files_path = images_files_not_deleted
        files_path.each do |file_hash_path|
            FileUtils.rm file_hash_path
        end if files_path.present?
    end

    private 
    def create_folder_on_disk
        directory = Rails.root.join('public',"#{UPLODAS_DIR}/#{folder}")
        FileUtils.mkdir_p(directory,mode: 0777) if !directory.directory?
        directory
    end

end
