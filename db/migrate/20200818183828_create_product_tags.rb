class CreateProductTags < ActiveRecord::Migration[6.0]
  def change
    create_table :product_tags do |t|
      t.bigint :product_id, unsigned: true, null: false
      t.string :tag_key, limit: 100, null: false
      t.text :tag_value
      t.timestamps
    end
  end
end
