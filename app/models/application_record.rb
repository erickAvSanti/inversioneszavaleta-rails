class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  public
  def random_string(size = 30)
    (0...size).map { ('a'..'z').to_a[rand(26)] }.join
  end

  def self.test_tmp
    tmp = nil
    [ENV['TMPDIR'], ENV['TMP'], ENV['TEMP'],  '/tmp', '.'].each do |dir|
      next if !dir
      dir = File.expand_path(dir)
      stat = File.stat(dir)
      puts dir
      puts "11" if stat.present?
      puts "22" if stat.directory?
      puts "33" if stat.writable?
      puts "44" if stat.world_writable?
      puts "55" if stat.sticky?
      if stat and stat.directory? and stat.writable? and
        (!stat.world_writable? or stat.sticky?)
        tmp = dir
        break
      end rescue nil
    end
    tmp
  end

end
