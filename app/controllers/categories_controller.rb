class CategoriesController < ApiController
  before_action :set_category, only: [:show, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    render json: Category.all_with_nested
  end

  # GET /categories/1
  # GET /categories/1.json
  #def show
    #@category = Category.find(prams[:id])
    #render json: @category
  #end

  # GET /categories/new
  #def new
    #@category = Category.new
  #end

  # GET /categories/1/edit
  #def edit
  #end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)
    if @category.save
      render json: nil, status: :created
    else
      render json: @category.errors, status: :not_acceptable
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    if @category.update(category_params)
      render json: nil, status: :ok
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    begin
      @category.destroy
      render json: {}
    rescue => exception
      render json: {}, status: :conflict
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def category_params
      params[:cat_name].upcase!
      permit_arr = [:cat_name]
      if params[:cat_id].present?
        parent = Category.find_by_id params[:cat_id]
        permit_arr << :cat_id if parent.present?
      end
      params.require(:category).permit(permit_arr)
    end
end
