class AuthorizationController < ApiController
    before_action :authorized, only: [:auto_login]
  
    def auto_login
        render json: @user
    end

    # LOGGING IN
    def login
        @user = User.find_by(username: params[:username])
    
        if @user && @user.authenticate(params[:password])
            encoded = encode_token({user_id: @user.id})
            render json: {user: @user.without_digest, token: encoded[:token], expires_in: encoded[:expires_in], expires_at: encoded[:expires_at]}
        else
            render json: {error: "Invalid username or password"}, status: :bad_request
        end
    end
end
