# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_28_172641) do

  create_table "categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "cat_name", limit: 70, null: false
    t.bigint "cat_id", unsigned: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cat_name"], name: "cat_name_unique", unique: true
  end

  create_table "product_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "product_id", null: false, unsigned: true
    t.string "file_name", limit: 120, null: false
    t.string "file_hash", limit: 100, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "img_dim_width", default: 0, null: false, unsigned: true
    t.integer "img_dim_height", default: 0, null: false, unsigned: true
    t.boolean "is_default", default: false, null: false
    t.index ["file_hash"], name: "file_hash_idx", unique: true
  end

  create_table "product_tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "product_id", null: false, unsigned: true
    t.string "tag_key", limit: 100, null: false
    t.text "tag_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "prod_name", limit: 100, null: false
    t.text "prod_desc", default: "''"
    t.decimal "prod_price", precision: 7, scale: 2, default: "0.0", null: false
    t.decimal "prod_price_off", precision: 7, scale: 2, default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "show_prod_price_off", default: false
    t.string "folder", limit: 100
    t.index ["prod_name"], name: "prod_name_idx"
  end

  create_table "products_categories", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "product_id", unsigned: true
    t.bigint "category_id", unsigned: true
    t.index ["product_id", "category_id"], name: "index_products_categories_on_product_id_and_category_id", unique: true
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "full_name", limit: 100, default: ""
  end

  create_table "web_properties", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "prop_key", limit: 50, null: false
    t.text "prop_desc"
    t.text "prop_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["prop_key"], name: "unique_prop_key", unique: true
  end

end
